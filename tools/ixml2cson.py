#!/usr/bin/env python3
"""
Convert ConTeXt interface XML into snippet-cson

Call: ixml2cson.py <TEXROOT> <output filename>
"""
import os
import sys
import cson
import subprocess
from pathlib import Path
import xml.etree.ElementTree as ET

xmlns_cd = '{http://www.pragma-ade.com/commands}'
outputname = Path('snippets/language-context.cson')
moreinformation = Path('tools/context-infos.cson')

def find_context():
    'find ConTeXt tree'
    if 'TEXROOT' in os.environ:
        # e.g. ~/texmf/tex
        return Path(os.environ['TEXROOT'])
    # TODO: search for texmf/tex in PATH?
    cp = subprocess.run('which mtxrun', shell=True, capture_output=True)
    # e.g. ~/texmf/tex/texmf-linux-64/bin/mtxrun
    if cp.stdout.strip():
        context = Path(cp.stdout.decode('utf-8').strip())
        return context.parents[2]
    return None

if len(sys.argv) > 1:
    ipath = Path(sys.argv[1])
    if not ipath.is_dir():
        print(f'Parameter "{ipath}" is not a directory.')
        sys.exit(2)
    if len(sys.argv) > 2:
        outputname = sys.argv[2]
else:
    ipath = find_context()
    if not ipath:
        print('ConTeXt path not found.')
        sys.exit(1)


# TODO: also look for module interface files
ipath = ipath / 'texmf-context/tex/context/interface/mkiv'
contents = list(ipath.glob('i-*.xml'))
if not len(contents):
    print(f'No ConTeXt interface files found in "{ipath}"')
    sys.exit(3)

# command description from wiki
INFO = {}
if moreinformation.is_file():
    with open(moreinformation, 'r') as infofile:
        INFO = cson.load(infofile)

# common definitions
# COMMONS = {}
# commondefs = ipath.glob('i-common-*.xml')
# for interface in commondefs:
#     if interface.name == 'i-common-definitions.xml':
#         continue
#     try:
#         tree = ET.parse(interface)
#     except ET.ParseError as ex:
#         print(ex)
#     root = tree.getroot()
#     for define in root.iter(xmlns_cd + 'define'):
#         name = define.attrib['name']
#         COMMONS[name] = define

instances = {}
try:
    tree = ET.parse(ipath / 'i-common-instance.xml')
except ET.ParseError as ex:
    print(ex)
root = tree.getroot()
for cmd in root.iter(xmlns_cd + 'define'):
    name = cmd.attrib['name']
    vals = []
    for cons in cmd:
        # constants
        vals.append(cons.attrib['value'])
    instances[name] = vals

commands = {}
for interface in contents:
    if 'common' in interface.name:
        continue
    print(interface.name)
    try:
        tree = ET.parse(interface)
    except ET.ParseError as ex:
        print(ex)
    root = tree.getroot()
    for cmd in root.iter(xmlns_cd + 'command'):
        name = cmd.attrib['name']
        names = [name,]
        if 'variant' in cmd.attrib \
        and (cmd.attrib['variant'].startswith('instance') \
        or cmd.attrib['variant']=='assignment'):
            # instances need to get resolved
            # e.g. note => footnote, endnote
            # e.g. section => chapter, section, subsection
            try:
                resolve = cmd.find('./' + xmlns_cd + 'instances/' + xmlns_cd + 'resolve').attrib['name']
                if not resolve in instances:
                    print('\t\tCan’t resolve %s/%s' % (name,resolve))
                    continue
                names = instances[resolve]
            except AttributeError as ex:
                print('\t\tCan’t find instances of %s' % (name))
        for this in names:
            if 'type' in cmd.attrib \
            and cmd.attrib['type']=='environment' \
            and not name.startswith('start'):
                this = 'start'+this
            body = this
            args = cmd.find(xmlns_cd + 'arguments')
            noofargs = 0
            if args:
                for child in args:
                    noofargs += 1
                    tag = child.tag.replace(xmlns_cd, '')
                    if tag=='assignments':
                        body += '[${%d:options}]' % noofargs
                    elif tag=='content':
                        body += '{${%d:content}}' % noofargs
                    elif tag=='resolve':
                        resolvename = child.attrib['name']
                        if 'keyword-name' in resolvename:
                            body += '[${%d:name}]' % noofargs
                        elif 'keyword-reference' in resolvename:
                            body += '[${%d:reference}]' % noofargs
                        elif resolvename=='argument-true':
                            body += '{${%d:content if true}}' % noofargs
                        elif resolvename=='argument-false':
                            body += '{${%d:content if false}}' % noofargs
                        elif resolvename.startswith('argument-'):
                            rest = resolvename.replace('argument-','')
                            body += '{${%d:%s}}' % (noofargs, rest)
                        elif resolvename.startswith('keyword-') or resolvename.startswith('string-'):
                            rest = resolvename.replace('keyword-','').replace('string-','')
                            #print('\t\tkeyword: \\%s[%s]' % (name, rest))
                            # [] make not always sense
                            body += '[${%d:%s}]' % (noofargs, rest)
                        elif 'floatdata-list' in resolvename:
                            body += '[${%s:title={},reference=,}]' % noofargs
                        elif resolvename.startswith('assignment'):
                            body += '[${%d:options}]' % noofargs
                        else:
                            print("\t\tUnhandled resolve in %s: %s" % (this, resolvename))
                            body += '[${%d:%s}]' % (noofargs, resolvename)
                    # TODO: sequence
            body += '$%d' % (noofargs + 1)
            if this.startswith('start'):
                stop = this.replace('start', 'stop')
                body += '\n\\\\%s\n' % stop
                # also add \stopsomething
                commands['\\'+stop] = {
                    'description': 'end of %s' % this,
                    'descriptionMoreURL': '',
                    'prefix': stop,
                    'body': stop
                }
            desc = ''
            if 'category' in cmd.attrib:
                desc = 'category: %s' % (cmd.attrib['category'])
                if 'level' in cmd.attrib:
                    desc += '; '
            if 'level' in cmd.attrib:
                desc += 'level: %s' % cmd.attrib['level']
            if 'file' in cmd.attrib:
                desc += '; defined in: %s' % cmd.attrib['file']
            desc += '; interface: %s' % interface.name
            if '\\'+this in INFO:
                idesc = INFO['\\'+this]['description']
                if idesc:
                    desc = '%s // %s' % (idesc, desc)
            # TODO: abstract wiki link for instances?
            commands['\\'+this] = {
                'description': desc,
                'descriptionMoreURL': 'https://wiki.contextgarden.net/Command/' + this,
                'prefix': this,
                'body': body
            }

#print(commands)
with open(outputname, 'w') as csonf:
    cson.dump({'.text.tex.context': commands}, csonf, sort_keys=True, indent=2)

print('"%s" written.' % outputname)
