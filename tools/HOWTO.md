# How to build

Since I always forget what I did…

* run `tools/ixml2cson.py` to create `snippets/autosnippets-context.cson`.
* don’t run `tools/extractinfos.py`, it’s outdated!
* run `tools/readwiki.py` to get information from the ConTeXt wiki.
* run `tools/ixml2cson.py` again to update `snippets/autosnippets-context.cson`.
* `tools/context-infos.cson` is most important
