# language-context-lmtx

This package adds support for the ConTeXt markup language in Pulsar/Atom, with syntax highlighting.

The list of commands is automatically created from ConTeXt LMTX interface XMLs and enriched with information from the [ConTeXt wiki](https://wiki.contextgarden.net/) using the included Python scripts.

---

## Install
The version of this repository is unpublished and will stay that way, while Pulsar only accepts contributions via GitHub. You can clone this repository to Pulsar’s package directory:

```
git clone https://codeberg.org/fiee/language-context-lmtx ~/.pulsar/packages/language-context-lmtx
```

## Optional dependencies

Recommended plugins:
* Support for TeX syntax highlighting: `language-tex` or `language-latex`
* Support for Lua syntax highlighting: `language-lua`
* File icons: `file-icons` (there are several, this contains a TeX icon)
* PDF viewer: `pdfjs-viewer` (there are several, this works for me); [enable SyncTeX support](https://github.com/allefeld/atom-pdfjs-viewer/issues/15) by editing `~/.pulsar/packages/pdfjs-viewer/lib/pdfjs-viewer-view.js`: uncomment line 71+72, add "var" in line 217. Then SyncTeX works with right-click, if you call ConTeXt with `--synctex=repeat`.

## Contributing
For modifications and discussion about the packet, please refer to this [repo](https://codeberg.org/fiee/language-context-lmtx).

## License
This package is released under the GPL license, v3 or better, see the attached [license file](https://codeberg.org/fiee/language-context-lmtx/blob/master/LICENSE) for details.

## Authors

* Patrick Gundlach made a [TextMate bundle](https://github.com/pgundlach/context.tmbundle) in about 2007.
* Enrico Lovisotto converted it to a [Atom plugin](https://github.com/lobisquit/language-context) in 2017.
* Massimilano Farinella [forked it](https://github.com/massifrg/language-context) in 2021 to update it, since E.L. didn’t respond to anything.
* Henning Hraban Ramm first collaborated with M.F. on updating, then decided to move to [Codeberg](https://codeberg.org/fiee/language-context-lmtx) in 2023 and finally renamed it in 2024 to be able to publish it as a new plugin.
